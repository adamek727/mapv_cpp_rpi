# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
SET(CMAKE_DEPENDS_CHECK_CXX
  "/home/harry/openCV_projects/MAPV/firstProgram/src/Clusterer.cpp" "/home/harry/openCV_projects/MAPV/firstProgram/CMakeFiles/TextSeg.dir/src/Clusterer.cpp.o"
  "/home/harry/openCV_projects/MAPV/firstProgram/src/Estimator.cpp" "/home/harry/openCV_projects/MAPV/firstProgram/CMakeFiles/TextSeg.dir/src/Estimator.cpp.o"
  "/home/harry/openCV_projects/MAPV/firstProgram/src/FuzzySegmenter.cpp" "/home/harry/openCV_projects/MAPV/firstProgram/CMakeFiles/TextSeg.dir/src/FuzzySegmenter.cpp.o"
  "/home/harry/openCV_projects/MAPV/firstProgram/src/GaborBankGenerator.cpp" "/home/harry/openCV_projects/MAPV/firstProgram/CMakeFiles/TextSeg.dir/src/GaborBankGenerator.cpp.o"
  "/home/harry/openCV_projects/MAPV/firstProgram/src/GaborFilterCell.cpp" "/home/harry/openCV_projects/MAPV/firstProgram/CMakeFiles/TextSeg.dir/src/GaborFilterCell.cpp.o"
  "/home/harry/openCV_projects/MAPV/firstProgram/src/GaborTextureSegmenter.cpp" "/home/harry/openCV_projects/MAPV/firstProgram/CMakeFiles/TextSeg.dir/src/GaborTextureSegmenter.cpp.o"
  "/home/harry/openCV_projects/MAPV/firstProgram/src/ImageNormalizer.cpp" "/home/harry/openCV_projects/MAPV/firstProgram/CMakeFiles/TextSeg.dir/src/ImageNormalizer.cpp.o"
  "/home/harry/openCV_projects/MAPV/firstProgram/src/main.cpp" "/home/harry/openCV_projects/MAPV/firstProgram/CMakeFiles/TextSeg.dir/src/main.cpp.o"
  )
SET(CMAKE_CXX_COMPILER_ID "GNU")

# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  )

# The include file search paths:
SET(CMAKE_C_TARGET_INCLUDE_PATH
  "/usr/local/include/opencv"
  "/usr/local/include"
  "include"
  )
SET(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
