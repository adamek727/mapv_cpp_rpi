# CMAKE generated file: DO NOT EDIT!
# Generated by "Unix Makefiles" Generator, CMake Version 2.8

#=============================================================================
# Special targets provided by cmake.

# Disable implicit rules so canonical targets will work.
.SUFFIXES:

# Remove some rules from gmake that .SUFFIXES does not remove.
SUFFIXES =

.SUFFIXES: .hpux_make_needs_suffix_list

# Suppress display of executed commands.
$(VERBOSE).SILENT:

# A target that is always out of date.
cmake_force:
.PHONY : cmake_force

#=============================================================================
# Set environment variables for the build.

# The shell in which to execute make rules.
SHELL = /bin/sh

# The CMake executable.
CMAKE_COMMAND = /usr/bin/cmake

# The command to remove a file.
RM = /usr/bin/cmake -E remove -f

# Escaping for special characters.
EQUALS = =

# The program to use to edit the cache.
CMAKE_EDIT_COMMAND = /usr/bin/ccmake

# The top-level source directory on which CMake was run.
CMAKE_SOURCE_DIR = /home/harry/openCV_projects/MAPV/firstProgram

# The top-level build directory on which CMake was run.
CMAKE_BINARY_DIR = /home/harry/openCV_projects/MAPV/firstProgram

# Include any dependencies generated for this target.
include CMakeFiles/TextSeg.dir/depend.make

# Include the progress variables for this target.
include CMakeFiles/TextSeg.dir/progress.make

# Include the compile flags for this target's objects.
include CMakeFiles/TextSeg.dir/flags.make

CMakeFiles/TextSeg.dir/src/main.cpp.o: CMakeFiles/TextSeg.dir/flags.make
CMakeFiles/TextSeg.dir/src/main.cpp.o: src/main.cpp
	$(CMAKE_COMMAND) -E cmake_progress_report /home/harry/openCV_projects/MAPV/firstProgram/CMakeFiles $(CMAKE_PROGRESS_1)
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Building CXX object CMakeFiles/TextSeg.dir/src/main.cpp.o"
	/usr/bin/c++   $(CXX_DEFINES) $(CXX_FLAGS) -o CMakeFiles/TextSeg.dir/src/main.cpp.o -c /home/harry/openCV_projects/MAPV/firstProgram/src/main.cpp

CMakeFiles/TextSeg.dir/src/main.cpp.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CXX source to CMakeFiles/TextSeg.dir/src/main.cpp.i"
	/usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -E /home/harry/openCV_projects/MAPV/firstProgram/src/main.cpp > CMakeFiles/TextSeg.dir/src/main.cpp.i

CMakeFiles/TextSeg.dir/src/main.cpp.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CXX source to assembly CMakeFiles/TextSeg.dir/src/main.cpp.s"
	/usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -S /home/harry/openCV_projects/MAPV/firstProgram/src/main.cpp -o CMakeFiles/TextSeg.dir/src/main.cpp.s

CMakeFiles/TextSeg.dir/src/main.cpp.o.requires:
.PHONY : CMakeFiles/TextSeg.dir/src/main.cpp.o.requires

CMakeFiles/TextSeg.dir/src/main.cpp.o.provides: CMakeFiles/TextSeg.dir/src/main.cpp.o.requires
	$(MAKE) -f CMakeFiles/TextSeg.dir/build.make CMakeFiles/TextSeg.dir/src/main.cpp.o.provides.build
.PHONY : CMakeFiles/TextSeg.dir/src/main.cpp.o.provides

CMakeFiles/TextSeg.dir/src/main.cpp.o.provides.build: CMakeFiles/TextSeg.dir/src/main.cpp.o

CMakeFiles/TextSeg.dir/src/Estimator.cpp.o: CMakeFiles/TextSeg.dir/flags.make
CMakeFiles/TextSeg.dir/src/Estimator.cpp.o: src/Estimator.cpp
	$(CMAKE_COMMAND) -E cmake_progress_report /home/harry/openCV_projects/MAPV/firstProgram/CMakeFiles $(CMAKE_PROGRESS_2)
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Building CXX object CMakeFiles/TextSeg.dir/src/Estimator.cpp.o"
	/usr/bin/c++   $(CXX_DEFINES) $(CXX_FLAGS) -o CMakeFiles/TextSeg.dir/src/Estimator.cpp.o -c /home/harry/openCV_projects/MAPV/firstProgram/src/Estimator.cpp

CMakeFiles/TextSeg.dir/src/Estimator.cpp.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CXX source to CMakeFiles/TextSeg.dir/src/Estimator.cpp.i"
	/usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -E /home/harry/openCV_projects/MAPV/firstProgram/src/Estimator.cpp > CMakeFiles/TextSeg.dir/src/Estimator.cpp.i

CMakeFiles/TextSeg.dir/src/Estimator.cpp.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CXX source to assembly CMakeFiles/TextSeg.dir/src/Estimator.cpp.s"
	/usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -S /home/harry/openCV_projects/MAPV/firstProgram/src/Estimator.cpp -o CMakeFiles/TextSeg.dir/src/Estimator.cpp.s

CMakeFiles/TextSeg.dir/src/Estimator.cpp.o.requires:
.PHONY : CMakeFiles/TextSeg.dir/src/Estimator.cpp.o.requires

CMakeFiles/TextSeg.dir/src/Estimator.cpp.o.provides: CMakeFiles/TextSeg.dir/src/Estimator.cpp.o.requires
	$(MAKE) -f CMakeFiles/TextSeg.dir/build.make CMakeFiles/TextSeg.dir/src/Estimator.cpp.o.provides.build
.PHONY : CMakeFiles/TextSeg.dir/src/Estimator.cpp.o.provides

CMakeFiles/TextSeg.dir/src/Estimator.cpp.o.provides.build: CMakeFiles/TextSeg.dir/src/Estimator.cpp.o

CMakeFiles/TextSeg.dir/src/Clusterer.cpp.o: CMakeFiles/TextSeg.dir/flags.make
CMakeFiles/TextSeg.dir/src/Clusterer.cpp.o: src/Clusterer.cpp
	$(CMAKE_COMMAND) -E cmake_progress_report /home/harry/openCV_projects/MAPV/firstProgram/CMakeFiles $(CMAKE_PROGRESS_3)
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Building CXX object CMakeFiles/TextSeg.dir/src/Clusterer.cpp.o"
	/usr/bin/c++   $(CXX_DEFINES) $(CXX_FLAGS) -o CMakeFiles/TextSeg.dir/src/Clusterer.cpp.o -c /home/harry/openCV_projects/MAPV/firstProgram/src/Clusterer.cpp

CMakeFiles/TextSeg.dir/src/Clusterer.cpp.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CXX source to CMakeFiles/TextSeg.dir/src/Clusterer.cpp.i"
	/usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -E /home/harry/openCV_projects/MAPV/firstProgram/src/Clusterer.cpp > CMakeFiles/TextSeg.dir/src/Clusterer.cpp.i

CMakeFiles/TextSeg.dir/src/Clusterer.cpp.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CXX source to assembly CMakeFiles/TextSeg.dir/src/Clusterer.cpp.s"
	/usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -S /home/harry/openCV_projects/MAPV/firstProgram/src/Clusterer.cpp -o CMakeFiles/TextSeg.dir/src/Clusterer.cpp.s

CMakeFiles/TextSeg.dir/src/Clusterer.cpp.o.requires:
.PHONY : CMakeFiles/TextSeg.dir/src/Clusterer.cpp.o.requires

CMakeFiles/TextSeg.dir/src/Clusterer.cpp.o.provides: CMakeFiles/TextSeg.dir/src/Clusterer.cpp.o.requires
	$(MAKE) -f CMakeFiles/TextSeg.dir/build.make CMakeFiles/TextSeg.dir/src/Clusterer.cpp.o.provides.build
.PHONY : CMakeFiles/TextSeg.dir/src/Clusterer.cpp.o.provides

CMakeFiles/TextSeg.dir/src/Clusterer.cpp.o.provides.build: CMakeFiles/TextSeg.dir/src/Clusterer.cpp.o

CMakeFiles/TextSeg.dir/src/GaborTextureSegmenter.cpp.o: CMakeFiles/TextSeg.dir/flags.make
CMakeFiles/TextSeg.dir/src/GaborTextureSegmenter.cpp.o: src/GaborTextureSegmenter.cpp
	$(CMAKE_COMMAND) -E cmake_progress_report /home/harry/openCV_projects/MAPV/firstProgram/CMakeFiles $(CMAKE_PROGRESS_4)
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Building CXX object CMakeFiles/TextSeg.dir/src/GaborTextureSegmenter.cpp.o"
	/usr/bin/c++   $(CXX_DEFINES) $(CXX_FLAGS) -o CMakeFiles/TextSeg.dir/src/GaborTextureSegmenter.cpp.o -c /home/harry/openCV_projects/MAPV/firstProgram/src/GaborTextureSegmenter.cpp

CMakeFiles/TextSeg.dir/src/GaborTextureSegmenter.cpp.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CXX source to CMakeFiles/TextSeg.dir/src/GaborTextureSegmenter.cpp.i"
	/usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -E /home/harry/openCV_projects/MAPV/firstProgram/src/GaborTextureSegmenter.cpp > CMakeFiles/TextSeg.dir/src/GaborTextureSegmenter.cpp.i

CMakeFiles/TextSeg.dir/src/GaborTextureSegmenter.cpp.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CXX source to assembly CMakeFiles/TextSeg.dir/src/GaborTextureSegmenter.cpp.s"
	/usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -S /home/harry/openCV_projects/MAPV/firstProgram/src/GaborTextureSegmenter.cpp -o CMakeFiles/TextSeg.dir/src/GaborTextureSegmenter.cpp.s

CMakeFiles/TextSeg.dir/src/GaborTextureSegmenter.cpp.o.requires:
.PHONY : CMakeFiles/TextSeg.dir/src/GaborTextureSegmenter.cpp.o.requires

CMakeFiles/TextSeg.dir/src/GaborTextureSegmenter.cpp.o.provides: CMakeFiles/TextSeg.dir/src/GaborTextureSegmenter.cpp.o.requires
	$(MAKE) -f CMakeFiles/TextSeg.dir/build.make CMakeFiles/TextSeg.dir/src/GaborTextureSegmenter.cpp.o.provides.build
.PHONY : CMakeFiles/TextSeg.dir/src/GaborTextureSegmenter.cpp.o.provides

CMakeFiles/TextSeg.dir/src/GaborTextureSegmenter.cpp.o.provides.build: CMakeFiles/TextSeg.dir/src/GaborTextureSegmenter.cpp.o

CMakeFiles/TextSeg.dir/src/GaborFilterCell.cpp.o: CMakeFiles/TextSeg.dir/flags.make
CMakeFiles/TextSeg.dir/src/GaborFilterCell.cpp.o: src/GaborFilterCell.cpp
	$(CMAKE_COMMAND) -E cmake_progress_report /home/harry/openCV_projects/MAPV/firstProgram/CMakeFiles $(CMAKE_PROGRESS_5)
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Building CXX object CMakeFiles/TextSeg.dir/src/GaborFilterCell.cpp.o"
	/usr/bin/c++   $(CXX_DEFINES) $(CXX_FLAGS) -o CMakeFiles/TextSeg.dir/src/GaborFilterCell.cpp.o -c /home/harry/openCV_projects/MAPV/firstProgram/src/GaborFilterCell.cpp

CMakeFiles/TextSeg.dir/src/GaborFilterCell.cpp.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CXX source to CMakeFiles/TextSeg.dir/src/GaborFilterCell.cpp.i"
	/usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -E /home/harry/openCV_projects/MAPV/firstProgram/src/GaborFilterCell.cpp > CMakeFiles/TextSeg.dir/src/GaborFilterCell.cpp.i

CMakeFiles/TextSeg.dir/src/GaborFilterCell.cpp.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CXX source to assembly CMakeFiles/TextSeg.dir/src/GaborFilterCell.cpp.s"
	/usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -S /home/harry/openCV_projects/MAPV/firstProgram/src/GaborFilterCell.cpp -o CMakeFiles/TextSeg.dir/src/GaborFilterCell.cpp.s

CMakeFiles/TextSeg.dir/src/GaborFilterCell.cpp.o.requires:
.PHONY : CMakeFiles/TextSeg.dir/src/GaborFilterCell.cpp.o.requires

CMakeFiles/TextSeg.dir/src/GaborFilterCell.cpp.o.provides: CMakeFiles/TextSeg.dir/src/GaborFilterCell.cpp.o.requires
	$(MAKE) -f CMakeFiles/TextSeg.dir/build.make CMakeFiles/TextSeg.dir/src/GaborFilterCell.cpp.o.provides.build
.PHONY : CMakeFiles/TextSeg.dir/src/GaborFilterCell.cpp.o.provides

CMakeFiles/TextSeg.dir/src/GaborFilterCell.cpp.o.provides.build: CMakeFiles/TextSeg.dir/src/GaborFilterCell.cpp.o

CMakeFiles/TextSeg.dir/src/FuzzySegmenter.cpp.o: CMakeFiles/TextSeg.dir/flags.make
CMakeFiles/TextSeg.dir/src/FuzzySegmenter.cpp.o: src/FuzzySegmenter.cpp
	$(CMAKE_COMMAND) -E cmake_progress_report /home/harry/openCV_projects/MAPV/firstProgram/CMakeFiles $(CMAKE_PROGRESS_6)
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Building CXX object CMakeFiles/TextSeg.dir/src/FuzzySegmenter.cpp.o"
	/usr/bin/c++   $(CXX_DEFINES) $(CXX_FLAGS) -o CMakeFiles/TextSeg.dir/src/FuzzySegmenter.cpp.o -c /home/harry/openCV_projects/MAPV/firstProgram/src/FuzzySegmenter.cpp

CMakeFiles/TextSeg.dir/src/FuzzySegmenter.cpp.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CXX source to CMakeFiles/TextSeg.dir/src/FuzzySegmenter.cpp.i"
	/usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -E /home/harry/openCV_projects/MAPV/firstProgram/src/FuzzySegmenter.cpp > CMakeFiles/TextSeg.dir/src/FuzzySegmenter.cpp.i

CMakeFiles/TextSeg.dir/src/FuzzySegmenter.cpp.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CXX source to assembly CMakeFiles/TextSeg.dir/src/FuzzySegmenter.cpp.s"
	/usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -S /home/harry/openCV_projects/MAPV/firstProgram/src/FuzzySegmenter.cpp -o CMakeFiles/TextSeg.dir/src/FuzzySegmenter.cpp.s

CMakeFiles/TextSeg.dir/src/FuzzySegmenter.cpp.o.requires:
.PHONY : CMakeFiles/TextSeg.dir/src/FuzzySegmenter.cpp.o.requires

CMakeFiles/TextSeg.dir/src/FuzzySegmenter.cpp.o.provides: CMakeFiles/TextSeg.dir/src/FuzzySegmenter.cpp.o.requires
	$(MAKE) -f CMakeFiles/TextSeg.dir/build.make CMakeFiles/TextSeg.dir/src/FuzzySegmenter.cpp.o.provides.build
.PHONY : CMakeFiles/TextSeg.dir/src/FuzzySegmenter.cpp.o.provides

CMakeFiles/TextSeg.dir/src/FuzzySegmenter.cpp.o.provides.build: CMakeFiles/TextSeg.dir/src/FuzzySegmenter.cpp.o

CMakeFiles/TextSeg.dir/src/GaborBankGenerator.cpp.o: CMakeFiles/TextSeg.dir/flags.make
CMakeFiles/TextSeg.dir/src/GaborBankGenerator.cpp.o: src/GaborBankGenerator.cpp
	$(CMAKE_COMMAND) -E cmake_progress_report /home/harry/openCV_projects/MAPV/firstProgram/CMakeFiles $(CMAKE_PROGRESS_7)
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Building CXX object CMakeFiles/TextSeg.dir/src/GaborBankGenerator.cpp.o"
	/usr/bin/c++   $(CXX_DEFINES) $(CXX_FLAGS) -o CMakeFiles/TextSeg.dir/src/GaborBankGenerator.cpp.o -c /home/harry/openCV_projects/MAPV/firstProgram/src/GaborBankGenerator.cpp

CMakeFiles/TextSeg.dir/src/GaborBankGenerator.cpp.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CXX source to CMakeFiles/TextSeg.dir/src/GaborBankGenerator.cpp.i"
	/usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -E /home/harry/openCV_projects/MAPV/firstProgram/src/GaborBankGenerator.cpp > CMakeFiles/TextSeg.dir/src/GaborBankGenerator.cpp.i

CMakeFiles/TextSeg.dir/src/GaborBankGenerator.cpp.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CXX source to assembly CMakeFiles/TextSeg.dir/src/GaborBankGenerator.cpp.s"
	/usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -S /home/harry/openCV_projects/MAPV/firstProgram/src/GaborBankGenerator.cpp -o CMakeFiles/TextSeg.dir/src/GaborBankGenerator.cpp.s

CMakeFiles/TextSeg.dir/src/GaborBankGenerator.cpp.o.requires:
.PHONY : CMakeFiles/TextSeg.dir/src/GaborBankGenerator.cpp.o.requires

CMakeFiles/TextSeg.dir/src/GaborBankGenerator.cpp.o.provides: CMakeFiles/TextSeg.dir/src/GaborBankGenerator.cpp.o.requires
	$(MAKE) -f CMakeFiles/TextSeg.dir/build.make CMakeFiles/TextSeg.dir/src/GaborBankGenerator.cpp.o.provides.build
.PHONY : CMakeFiles/TextSeg.dir/src/GaborBankGenerator.cpp.o.provides

CMakeFiles/TextSeg.dir/src/GaborBankGenerator.cpp.o.provides.build: CMakeFiles/TextSeg.dir/src/GaborBankGenerator.cpp.o

CMakeFiles/TextSeg.dir/src/ImageNormalizer.cpp.o: CMakeFiles/TextSeg.dir/flags.make
CMakeFiles/TextSeg.dir/src/ImageNormalizer.cpp.o: src/ImageNormalizer.cpp
	$(CMAKE_COMMAND) -E cmake_progress_report /home/harry/openCV_projects/MAPV/firstProgram/CMakeFiles $(CMAKE_PROGRESS_8)
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Building CXX object CMakeFiles/TextSeg.dir/src/ImageNormalizer.cpp.o"
	/usr/bin/c++   $(CXX_DEFINES) $(CXX_FLAGS) -o CMakeFiles/TextSeg.dir/src/ImageNormalizer.cpp.o -c /home/harry/openCV_projects/MAPV/firstProgram/src/ImageNormalizer.cpp

CMakeFiles/TextSeg.dir/src/ImageNormalizer.cpp.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CXX source to CMakeFiles/TextSeg.dir/src/ImageNormalizer.cpp.i"
	/usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -E /home/harry/openCV_projects/MAPV/firstProgram/src/ImageNormalizer.cpp > CMakeFiles/TextSeg.dir/src/ImageNormalizer.cpp.i

CMakeFiles/TextSeg.dir/src/ImageNormalizer.cpp.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CXX source to assembly CMakeFiles/TextSeg.dir/src/ImageNormalizer.cpp.s"
	/usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -S /home/harry/openCV_projects/MAPV/firstProgram/src/ImageNormalizer.cpp -o CMakeFiles/TextSeg.dir/src/ImageNormalizer.cpp.s

CMakeFiles/TextSeg.dir/src/ImageNormalizer.cpp.o.requires:
.PHONY : CMakeFiles/TextSeg.dir/src/ImageNormalizer.cpp.o.requires

CMakeFiles/TextSeg.dir/src/ImageNormalizer.cpp.o.provides: CMakeFiles/TextSeg.dir/src/ImageNormalizer.cpp.o.requires
	$(MAKE) -f CMakeFiles/TextSeg.dir/build.make CMakeFiles/TextSeg.dir/src/ImageNormalizer.cpp.o.provides.build
.PHONY : CMakeFiles/TextSeg.dir/src/ImageNormalizer.cpp.o.provides

CMakeFiles/TextSeg.dir/src/ImageNormalizer.cpp.o.provides.build: CMakeFiles/TextSeg.dir/src/ImageNormalizer.cpp.o

# Object files for target TextSeg
TextSeg_OBJECTS = \
"CMakeFiles/TextSeg.dir/src/main.cpp.o" \
"CMakeFiles/TextSeg.dir/src/Estimator.cpp.o" \
"CMakeFiles/TextSeg.dir/src/Clusterer.cpp.o" \
"CMakeFiles/TextSeg.dir/src/GaborTextureSegmenter.cpp.o" \
"CMakeFiles/TextSeg.dir/src/GaborFilterCell.cpp.o" \
"CMakeFiles/TextSeg.dir/src/FuzzySegmenter.cpp.o" \
"CMakeFiles/TextSeg.dir/src/GaborBankGenerator.cpp.o" \
"CMakeFiles/TextSeg.dir/src/ImageNormalizer.cpp.o"

# External object files for target TextSeg
TextSeg_EXTERNAL_OBJECTS =

TextSeg: CMakeFiles/TextSeg.dir/src/main.cpp.o
TextSeg: CMakeFiles/TextSeg.dir/src/Estimator.cpp.o
TextSeg: CMakeFiles/TextSeg.dir/src/Clusterer.cpp.o
TextSeg: CMakeFiles/TextSeg.dir/src/GaborTextureSegmenter.cpp.o
TextSeg: CMakeFiles/TextSeg.dir/src/GaborFilterCell.cpp.o
TextSeg: CMakeFiles/TextSeg.dir/src/FuzzySegmenter.cpp.o
TextSeg: CMakeFiles/TextSeg.dir/src/GaborBankGenerator.cpp.o
TextSeg: CMakeFiles/TextSeg.dir/src/ImageNormalizer.cpp.o
TextSeg: CMakeFiles/TextSeg.dir/build.make
TextSeg: /usr/local/lib/libopencv_videostab.so.3.1.0
TextSeg: /usr/local/lib/libopencv_videoio.so.3.1.0
TextSeg: /usr/local/lib/libopencv_video.so.3.1.0
TextSeg: /usr/local/lib/libopencv_superres.so.3.1.0
TextSeg: /usr/local/lib/libopencv_stitching.so.3.1.0
TextSeg: /usr/local/lib/libopencv_shape.so.3.1.0
TextSeg: /usr/local/lib/libopencv_photo.so.3.1.0
TextSeg: /usr/local/lib/libopencv_objdetect.so.3.1.0
TextSeg: /usr/local/lib/libopencv_ml.so.3.1.0
TextSeg: /usr/local/lib/libopencv_imgproc.so.3.1.0
TextSeg: /usr/local/lib/libopencv_imgcodecs.so.3.1.0
TextSeg: /usr/local/lib/libopencv_highgui.so.3.1.0
TextSeg: /usr/local/lib/libopencv_flann.so.3.1.0
TextSeg: /usr/local/lib/libopencv_features2d.so.3.1.0
TextSeg: /usr/local/lib/libopencv_core.so.3.1.0
TextSeg: /usr/local/lib/libopencv_calib3d.so.3.1.0
TextSeg: /usr/local/lib/libopencv_features2d.so.3.1.0
TextSeg: /usr/local/lib/libopencv_ml.so.3.1.0
TextSeg: /usr/local/lib/libopencv_highgui.so.3.1.0
TextSeg: /usr/local/lib/libopencv_videoio.so.3.1.0
TextSeg: /usr/local/lib/libopencv_imgcodecs.so.3.1.0
TextSeg: /usr/local/lib/libopencv_flann.so.3.1.0
TextSeg: /usr/local/lib/libopencv_video.so.3.1.0
TextSeg: /usr/local/lib/libopencv_imgproc.so.3.1.0
TextSeg: /usr/local/lib/libopencv_core.so.3.1.0
TextSeg: CMakeFiles/TextSeg.dir/link.txt
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --red --bold "Linking CXX executable TextSeg"
	$(CMAKE_COMMAND) -E cmake_link_script CMakeFiles/TextSeg.dir/link.txt --verbose=$(VERBOSE)

# Rule to build all files generated by this target.
CMakeFiles/TextSeg.dir/build: TextSeg
.PHONY : CMakeFiles/TextSeg.dir/build

CMakeFiles/TextSeg.dir/requires: CMakeFiles/TextSeg.dir/src/main.cpp.o.requires
CMakeFiles/TextSeg.dir/requires: CMakeFiles/TextSeg.dir/src/Estimator.cpp.o.requires
CMakeFiles/TextSeg.dir/requires: CMakeFiles/TextSeg.dir/src/Clusterer.cpp.o.requires
CMakeFiles/TextSeg.dir/requires: CMakeFiles/TextSeg.dir/src/GaborTextureSegmenter.cpp.o.requires
CMakeFiles/TextSeg.dir/requires: CMakeFiles/TextSeg.dir/src/GaborFilterCell.cpp.o.requires
CMakeFiles/TextSeg.dir/requires: CMakeFiles/TextSeg.dir/src/FuzzySegmenter.cpp.o.requires
CMakeFiles/TextSeg.dir/requires: CMakeFiles/TextSeg.dir/src/GaborBankGenerator.cpp.o.requires
CMakeFiles/TextSeg.dir/requires: CMakeFiles/TextSeg.dir/src/ImageNormalizer.cpp.o.requires
.PHONY : CMakeFiles/TextSeg.dir/requires

CMakeFiles/TextSeg.dir/clean:
	$(CMAKE_COMMAND) -P CMakeFiles/TextSeg.dir/cmake_clean.cmake
.PHONY : CMakeFiles/TextSeg.dir/clean

CMakeFiles/TextSeg.dir/depend:
	cd /home/harry/openCV_projects/MAPV/firstProgram && $(CMAKE_COMMAND) -E cmake_depends "Unix Makefiles" /home/harry/openCV_projects/MAPV/firstProgram /home/harry/openCV_projects/MAPV/firstProgram /home/harry/openCV_projects/MAPV/firstProgram /home/harry/openCV_projects/MAPV/firstProgram /home/harry/openCV_projects/MAPV/firstProgram/CMakeFiles/TextSeg.dir/DependInfo.cmake --color=$(COLOR)
.PHONY : CMakeFiles/TextSeg.dir/depend

