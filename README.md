
# Texture segmentation project

This project aiming at texture segmentation on images captured by raspberry pi camera.
Solution is based on OpenCV 3 framework.
For more informations see http://midas.uamt.feec.vutbr.cz/APV/apv_cz.php

## Authors 
Ligocki, Lázna
